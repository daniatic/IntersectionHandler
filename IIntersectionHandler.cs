﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntersectionHandler {
  public interface IIntersectionHandler {
    bool IntersectsWith(ITimeSet timeSet);
  }
}
