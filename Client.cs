﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntersectionHandler {
  class Client {
    void Program() {
      ITimeSet timeSetA = new TimeSetA();
      ITimeSet timeSetB = new TimeSetB();
      IntersectionHandler.Intersects(timeSetA, timeSetB);
      IntersectionHandler.Intersects(timeSetA, timeSetA);
      IntersectionHandler.Intersects(timeSetB, timeSetB);
      IntersectionHandler.Intersects(timeSetB, timeSetA);
    }
  }
}
