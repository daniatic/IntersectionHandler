﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntersectionHandler {
  public class IntersectionHandler : ITimeSetVisitor {

    public static bool Intersects(ITimeSet timeSet1, ITimeSet timeSet2) {
      IntersectionHandler intersectionHandler = new IntersectionHandler();
      timeSet1.Accept(intersectionHandler);
      return intersectionHandler.intersectionHandler.IntersectsWith(timeSet2);
    }

    private IIntersectionHandler intersectionHandler;

    public void Visit(TimeSetA timeSetA) {
      intersectionHandler = new IntersectionHandlerA(timeSetA);
    }

    public void Visit(TimeSetB timeSetB) {
      intersectionHandler = new IntersectionHandlerB(timeSetB);
    }
  }

}
