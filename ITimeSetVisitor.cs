﻿namespace IntersectionHandler {
  public interface ITimeSetVisitor {
    void Visit(TimeSetA timeSetA);
    void Visit(TimeSetB timeSetB);
  }
}