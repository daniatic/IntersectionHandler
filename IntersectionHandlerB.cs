﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntersectionHandler {
  public class IntersectionHandlerB : ITimeSetVisitor, IIntersectionHandler {
    TimeSetB timeSetB;
    bool intersects;

    public IntersectionHandlerB(TimeSetB timeSetB) {
      this.timeSetB = timeSetB;
    }
    public bool IntersectsWith(ITimeSet timeSet) {
      timeSet.Accept(this);
      return intersects;
    }

    public void Visit(TimeSetA timeSetA) {
      intersects = IntersectionHandlerAB.Intersects(timeSetA, this.timeSetB);
    }

    public void Visit(TimeSetB timeSetB) {
      intersects = IntersectionHandlerBB.Intersects(this.timeSetB, timeSetB);
    }
  }
}
