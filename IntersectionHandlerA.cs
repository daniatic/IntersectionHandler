﻿namespace IntersectionHandler {
  public class IntersectionHandlerA : ITimeSetVisitor, IIntersectionHandler {
    TimeSetA timeSetA;
    bool intersects;

    public IntersectionHandlerA(TimeSetA timeSetA) {
      this.timeSetA = timeSetA;
    }
    public bool IntersectsWith(ITimeSet timeSet) {
      timeSet.Accept(this);
      return intersects;
    }

    public void Visit(TimeSetA timeSetA) {
      intersects = IntersectionHandlerAA.Intersects(this.timeSetA, timeSetA);
    }

    public void Visit(TimeSetB timeSetB) {
      intersects = IntersectionHandlerAB.Intersects(timeSetA, timeSetB);
    }
  }

}
